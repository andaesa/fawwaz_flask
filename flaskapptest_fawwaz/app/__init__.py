from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
#app.config.from_object('config')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://falcon:tiger@localhost/mydatabase'
app.config['SECRET_KEY'] = 'secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/mydatabase'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://andaesa:falcon@localhost/mydatabase'
app.config['SQLALCHEMY_ECHO'] = True
app.config['WTF_CSRF_SECRET_KEY'] = 'random'

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'

login_manager.init_app(app)

from app import models, views