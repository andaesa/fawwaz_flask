from flask import render_template, request, url_for, redirect, flash
from flask_wtf import Form
from flask_login import login_user, logout_user, login_required
import paho.mqtt.client as mqtt
from app import app, db
from models import User, Data


@app.route('/')
def index():
	return render_template('index.html')
	
@app.route('/login', methods=['GET','POST'])
def login():
	if request.method == 'GET':
		return render_template('login.html')
	username = request.form['username']
	password = request.form['password']

	registered_user = User.query.filter_by(username=username, password=password).first()

	if registered_user is None:
		return redirect(url_for('login'))

	login_user(registered_user)
	return render_template('user_home.html')


@app.route('/register', methods=['GET','POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html')

	user = User(request.form['username'], request.form['password'], request.form['email'])
	db.session.add(user)
	db.session.commit()

	return render_template('register_success.html')


@app.route('/user_home')
@login_required
def user_home():
	return render_template('user_home.html')

@app.route('/1')
@login_required
def one():
	return render_template('one.html')

@app.route('/2')
@login_required
def two():
	return render_template('two.html')

@app.route('/3')
@login_required
def three():
	return render_template('three.html')

### MQTT code to receive message and store it into the database ### 
def on_connect(client, userdata, rc):
	print("connected with result code" + str(rc))

	client.subscribe("abc123")

def on_message(client, userdata, msg):
	messagedata = Data(temperature=msg.temperature, pH=msg.pH, time=msg.time)

	db.session.add(messagedata)
	db.session.commit()
###   -------------------------------------------------------- ###

@app.route('/table')
@login_required
def table_data():
	messages = Data.query.all()
	return render_template('table_data.html', messages=messages)


@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))



	


	



	