
#import paho.mqtt.client as mqtt
from app import app, db, login_manager

# Table User's configuration
class User(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), unique=True, index=True)
	password = db.Column(db.String(128))
	email = db.Column(db.String(120), unique=True, index=True)

	def __init__(self, username, password, email):
		self.username = username
		self.password = password
		self.email = email

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.id)

	def __repr__(self):
		return '<User %r>' % (self.username)

# Table Data's configuration for mqtt
class Data(db.Model):
	__tablename__= 'data_reading'
	id = db.Column(db.Integer, primary_key=True)
	temperature = db.Column(db.Integer, index=True)
	pH = db.Column(db.Integer, index=True)
	time = db.Column(db.DateTime, index=True)

	def __init__(self, temperature, pH, time):
		self.temperature = temperature
		self.pH = pH
		self.time = time


db.create_all()


@login_manager.user_loader
def load_user(id):
	return User.query.get(int(id))



